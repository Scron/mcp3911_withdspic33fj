
#include <stdlib.h>
#include <stdio.h>
#include "main.h"
#include "spi.h"
#include "ds3234.h"
//#include "delay.h"
#include "LCD.h"
#define DELAY 120
//declare external variable in main.c  

rtc_t real_time;

/* utility */
void Delay(unsigned long delay_count)
{
    volatile unsigned long temp;
    for (temp = delay_count; temp > 0; temp--);

}

/* convert BCD to HEX */
unsigned char utilBcd2Hex(unsigned char bcd)
{
    unsigned char hex;
    unsigned char a, b;
    a = bcd & 0x0F;
    b = (bcd >> 4)&0x0F;
    hex = b * 10 + a;
    return hex;
}

/* convert HEX to BCD */
unsigned char utilHex2Bcd(unsigned char hex)
{
    unsigned char bcd;

    bcd = ((hex / 10) << 4) + (hex % 10);
    return bcd;
}

unsigned char getRTCSecond(unsigned char *sec)
{
    unsigned char temp_sec;
    SPICS_DS3234 = CHIP_SEL_ENABLE;
    writeSPI2Byte(DS3234_REG_SECOND);
    Delay(DELAY);
    writeSPI2Byte(0x00); //get extra 8 clocks to read a byte from DS3234
    Delay(DELAY);
    temp_sec = readSPI2Byte();
    SPICS_DS3234 = CHIP_SEL_DISABLE;
    if (sec != NULL) {
        *sec = temp_sec;
    }
    return temp_sec;
}

unsigned char getRTCMinute(unsigned char *min)
{
    unsigned char temp_min;

    SPICS_DS3234 = CHIP_SEL_ENABLE;
    writeSPI2Byte(DS3234_REG_MINUTE);
    Delay(DELAY);
    writeSPI2Byte(0x00); //get extra 8 clocks to read a byte from DS3234
    Delay(DELAY);
    temp_min = readSPI2Byte();
    SPICS_DS3234 = CHIP_SEL_DISABLE;
    if (min != NULL) {
        *min = temp_min;
    }
    return temp_min;
}

unsigned char getRTCHour(unsigned char *hour)
{
    unsigned char temp_hour;

    SPICS_DS3234 = CHIP_SEL_ENABLE;
    writeSPI2Byte(DS3234_REG_HOUR);
    Delay(DELAY);
    writeSPI2Byte(0x00); //get extra 8 clocks to read a byte from DS3234
    Delay(DELAY);
    temp_hour = readSPI2Byte();
    if (hour != NULL) {
        *hour = temp_hour;
    }
    SPICS_DS3234 = CHIP_SEL_DISABLE;

    return temp_hour;
}

unsigned char getRTCDay(unsigned char *day)
{
    /* your turn */
}

unsigned char getRTCDate(unsigned char *date)
{
    /* your turn */
}

unsigned char getRTCMonth(unsigned char *month)
{
    /* your turn */
}

unsigned char getRTCYear(unsigned char *yr)
{
    /* your turn */
}

void readRTC(rtc_t *rtc_time)
{
    // read second
    getRTCSecond(&rtc_time->second);
    Delay(2);

    // read minute
    getRTCMinute(&rtc_time->minute);
    //Minute_D = (Minute>>4)*10 + (Minute%16);
    Delay(2);

    // read hour
    getRTCHour(&rtc_time->hour);
    //HhMm = (Hour << 8) + Minute;
    Delay(2);

    // read day
    getRTCDay(&rtc_time->day);
    Delay(2);

    // read date
    getRTCDate(&rtc_time->date);
    Delay(2);

    // read month
    getRTCMonth(&rtc_time->month);
    Delay(2);

    // read year
    getRTCYear(&rtc_time->year);
    Delay(2);
}

//void Show_RTC_Date(void)

void showRTCDate(rtc_t *rtc_time)
{
    char LCD_Data[16] = {0};
    LCD_Cursor_New(1, 0);
    sprintf(LCD_Data, "%02d/%02d/%02d", utilBcd2Hex(rtc_time->year),
                                        utilBcd2Hex(rtc_time->month),
                                        utilBcd2Hex(rtc_time->date));
    putsLCD((unsigned char*) LCD_Data);
    LCD_Cursor_New(0, 0);
    switch (utilBcd2Hex(rtc_time->day)) {
    case 1:
        putsLCD((unsigned char*) "Monday");
        break;
    case 2:
        putsLCD((unsigned char*) "Tuesday");
        break;
    case 3:
        putsLCD((unsigned char*) "Wednesday");
        break;
    case 4:
        putsLCD((unsigned char*) "Thursday");
        break;
    case 5:
        putsLCD((unsigned char*) "Friday");
        break;
    case 6:
        putsLCD((unsigned char*) "Saturday");
        break;
    case 7:
        putsLCD((unsigned char*) "Sunday");
        break;
    }
}

//void Show_RTC_Time(void)

void showRTCTime(rtc_t *rtc_time)
{
    char LCD_Data[20];
    LCD_Cursor_New(1, 10);
    if ((rtc_time->hour)&0x40) { // AM/PM 0~12 

        if ((rtc_time->hour)&0x20) {
            sprintf(LCD_Data, "PM%02d:%02d:%02d", utilBcd2Hex(rtc_time->hour),
                    utilBcd2Hex(rtc_time->minute),
                    utilBcd2Hex(rtc_time->second));
        } else {
            sprintf(LCD_Data, "AM%02d:%02d:%02d", utilBcd2Hex(rtc_time->hour),
                    utilBcd2Hex(rtc_time->minute),
                    utilBcd2Hex(rtc_time->second));
        }
    } else {
        sprintf(LCD_Data, "  %02d:%02d:%02d",
                utilBcd2Hex(rtc_time->hour),
                utilBcd2Hex(rtc_time->minute),
                utilBcd2Hex(rtc_time->second));

    }
    putsLCD((unsigned char*) LCD_Data);
}

//void Show_RTC(void)

void showRTC(rtc_t *rtc_time)
{
    showRTCDate(rtc_time);
    showRTCTime(rtc_time);
}

static void _setRTCDisable(void)
{
    SPICS_DS3234 = CHIP_SEL_DISABLE;
    Delay(12);
    writeSPI2Byte(DS3234_REG_CONTROL | SPI_WRITE);
    Delay(12);
    writeSPI2Byte(REG_CTRL_BIT_EOSC);
    Delay(12);
    SPICS_DS3234 = CHIP_SEL_DISABLE;
}

static void _setRTCEnable(void)
{
    SPICS_DS3234 = CHIP_SEL_ENABLE;
    Delay(12);
    writeSPI2Byte(DS3234_REG_CONTROL | SPI_WRITE);
    Delay(12);
    writeSPI2Byte(REG_CTRL_BIT_BBSQW | REG_CTRL_BIT_CONV);
    Delay(12);
    SPICS_DS3234 = CHIP_SEL_DISABLE;
}

int setRTCSecond(unsigned char sec)
{


    SPICS_DS3234 = CHIP_SEL_ENABLE;
    writeSPI2Byte(DS3234_REG_SECOND | SPI_WRITE);
    Delay(DELAY);
    writeSPI2Byte(sec); //get extra 8 clocks to read a byte from DS3234
    Delay(DELAY);
    SPICS_DS3234 = CHIP_SEL_DISABLE;



    return RTC_OK;
}

int setRTCMinute(unsigned char min)
{


    SPICS_DS3234 = CHIP_SEL_ENABLE;
    writeSPI2Byte(DS3234_REG_MINUTE | SPI_WRITE);
    Delay(DELAY);
    writeSPI2Byte(min); //get extra 8 clocks to read a byte from DS3234
    Delay(DELAY);
    SPICS_DS3234 = CHIP_SEL_DISABLE;



    return RTC_OK;
}

int setRTCHour(unsigned char hour)
{


    //set hour
    SPICS_DS3234 = CHIP_SEL_ENABLE;
    writeSPI2Byte(DS3234_REG_HOUR | SPI_WRITE);
    Delay(DELAY);
    writeSPI2Byte(hour); //get extra 8 clocks to read a byte from DS3234
    Delay(DELAY);
    SPICS_DS3234 = CHIP_SEL_DISABLE;



    return RTC_OK;
}

int setRTCDay(unsigned char day)
{

    SPICS_DS3234 = CHIP_SEL_ENABLE;
    writeSPI2Byte(0x83);
    while (SPI2STATbits.SPITBF == 1);
    writeSPI2Byte(day); //get extra 8 clocks to read a byte from DS3234
    Delay(DELAY);
    SPICS_DS3234 = CHIP_SEL_DISABLE;



    return RTC_OK;
}

int setRTCDate(unsigned char date)
{

    SPICS_DS3234 = CHIP_SEL_ENABLE;
    writeSPI2Byte(DS3234_REG_DATE | SPI_WRITE);
    Delay(DELAY);
    writeSPI2Byte(date); //get extra 8 clocks to read a byte from DS3234
    Delay(DELAY);
    SPICS_DS3234 = CHIP_SEL_DISABLE;



    return RTC_OK;
}

int setRTCMonth(unsigned char month)
{



    SPICS_DS3234 = CHIP_SEL_ENABLE;
    writeSPI2Byte(DS3234_REG_MONTH | SPI_WRITE);
    Delay(DELAY);
    writeSPI2Byte(month); //get extra 8 clocks to read a byte from DS3234
    Delay(DELAY);
    SPICS_DS3234 = CHIP_SEL_DISABLE;



    return RTC_OK;
}

int setRTCYear(unsigned char yr)
{


    //set year
    SPICS_DS3234 = CHIP_SEL_ENABLE;
    writeSPI2Byte(DS3234_REG_YEAR | SPI_WRITE);
    Delay(DELAY);
    writeSPI2Byte(yr); //get extra 8 clocks to read a byte from DS3234
    Delay(DELAY);
    SPICS_DS3234 = CHIP_SEL_DISABLE;



    return RTC_OK;
}

//SET DS3234 RTC Run

void setRTCRun(void)
{
    //_setRTCDisable();
    _setRTCEnable();
}