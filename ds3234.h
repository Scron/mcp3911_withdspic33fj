#ifndef DS3234_H
#define	DS3234_H
#include <p33FXXXX.h>

#define RTC_OK                      0
#define RTC_FAIL                    -1

#define DS3234_REG_SECOND           0x00
#define DS3234_REG_MINUTE           0x01
#define DS3234_REG_HOUR             0x02
#define DS3234_REG_DAY              0x03
#define DS3234_REG_DATE             0x04
#define DS3234_REG_MONTH            0x05
#define DS3234_REG_YEAR             0x06
#define DS3234_REG_ALM1_SEC         0x07
#define DS3234_REG_ALM1_MIN         0x08
#define DS3234_REG_ALM1_HOUR        0x09
#define DS3234_REG_ALM1_DAY         0x0A
#define DS3234_REG_ALM2_MIN         0x0B
#define DS3234_REG_ALM2_HOUR        0x0C
#define DS3234_REG_ALM2_DAY         0x0D
#define DS3234_REG_CONTROL          0x0E
#define DS3234_REG_STATUS           0x0F
#define DS3234_REG_AGING_OFFSET     0x10
#define DS3234_REG_TEMP_MSB         0x11
#define DS3234_REG_TEMP_LSB         0x12
#define DS3234_REG_TEMP_CONV        0x13
#define DS3234_REG_SRAM_ADDR        0x18
#define DS3234_REG_SRAM_DATA        0x19

/* Control Register 0x0E */
#define REG_CTRL_BIT_EOSC           0x80
#define REG_CTRL_BIT_BBSQW          0x40
#define REG_CTRL_BIT_CONV           0x20
#define REG_CTRL_BIT_RS2            0x10
#define REG_CTRL_BIT_RS1            0x08
#define REG_CTRL_BIT_INTCN          0x04
#define REG_CTRL_BIT_A2IE           0x02
#define REG_CTRL_BIT_A1IE           0x01

typedef struct rtc {
    unsigned char   year;               /* in BCD */
    unsigned char   month;              /* in BCD */
    unsigned char   date;               /* in BCD */
    unsigned char   day;                /* in BCD */
    unsigned char   hour;               /* in BCD */
    unsigned char   minute;             /* in BCD */
    unsigned char   second;             /* in BCD */
} rtc_t;

extern rtc_t            real_time;
unsigned char utilBcd2Hex(unsigned char bcd);
unsigned char utilHex2Bcd(unsigned char hex);
unsigned char getRTCSecond(unsigned char *sec);
unsigned char getRTCMinute(unsigned char *min);
unsigned char getRTCHour(unsigned char *hour);
unsigned char getRTCDate(unsigned char *date);
unsigned char getRTCMonth(unsigned char *month);
unsigned char getRTCYear(unsigned char *yr);
void readRTC(rtc_t *rtc_time);
int setRTCSecond(unsigned char sec);
int setRTCMinute(unsigned char min);
int setRTCHour(unsigned char hour);
int setRTCDay(unsigned char day);
int setRTCDate(unsigned char date);
int setRTCMonth(unsigned char month);
int setRTCYear(unsigned char yr);
void setRTCRun(void);
void setRTC(char Date_Time[12]);
void showRTCDate(rtc_t *rtc_time);
void showRTCTime(rtc_t *rtc_time);
void showRTC(rtc_t *rtc_time);

#endif	/* DS3234_H */

