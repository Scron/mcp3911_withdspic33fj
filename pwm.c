#include <xc.h>
#include <stdio.h>
#include "main.h"

int initPWM(void)
{
    TRISE = 0x00;  // make sure PWM pins are set to be outputs
    PORTE = 0x00; // clear the outputs

    /* Configuration register FPOR */
    /* High and Low switches set to active-high state */
//    _FPOR(RST_PWMPIN & PWMxH_ACT_HI & PWMxL_ACT_HI)
    /* PWM time base operates in a Free Running mode */
    P1TCONbits.PTMOD = 0b00;
    /* PWM time base input clock period is TCY(1:1 prescale) */
   
    P1TCONbits.PTCKPS = 0b00;            /* PWM time base output post scale is 1:1 */
    P1TCONbits.PTOPS = 0b00;
    /* Choose PWM time period based on input clock selected */
    /* Refer to Equation 14-1*/
    /* PWM switching frequency is 40 kHz */
    /* FCYis 40 MHz */
    P1TPER = 999;
    P1TMR = 0;

    /* Set PWM I/O pin */
    PWM1CON1bits.PMOD1 = 1;             /* PWM I/O pin pair is in Independent Output mode*/
    PWM1CON1bits.PEN1H = 1;             /* PWM pin is enabled for PWM output*/
    PWM1CON1bits.PEN1L = 0;             /* PWM pin is disabled for PWM output*/

    /* Immediate update of PWM enabled */
    PWM1CON2bits.IUE = 1;               /* Duty cycle immediate update is enable */

    /* Initialize duty cycle values for PWM1, PWM2 and PWM3 signals */
    P1DC1 = 999;

    P1TCONbits.PTEN = 1;
    return 0;
}

void initMP3911CLK(void)
{
    P1TPER = 9;
    P1DC1 = 9;
}


