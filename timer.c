#include <xc.h>
#include "main.h"
#include "timer.h"

unsigned int    t1_counter = 0 ;
unsigned int    time_sec = 0;

void T1_Initial(void)
{
    /* this timer 1 setup for interrupt every 1us */
    PR1 = 5;
    T1CON = 0x00;
    T1CONbits.TCKPS = 1;    /* prescale = 1:8 */
    T1CONbits.TON = 0;     
    IFS0bits.T1IF = 0;
    IEC0bits.T1IE = 1;
}

inline void T1_StartTiming(void)
{
    T1CONbits.TON = 1;
}

inline void T1_EndTiming(void)
{
    T1CONbits.TON = 0;
}

void __attribute__((interrupt, auto_psv)) _T1Interrupt(void)
{
    IFS0bits.T1IF = 0;                  // clear Timer1 interrupt flag
    t1_counter++;
}
