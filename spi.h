#ifndef SPI_H
#define	SPI_H

/********************************************************************/
/*              Header for SPI module library functions             */
/********************************************************************/
void initSPI1(void);
void initSPI2(void);
unsigned char readSPI1Byte(void);
unsigned char readSPI2Byte(void);
void  writeSPI1Byte(unsigned int);
void  writeSPI2Byte(unsigned int);

#define CHIP_SEL_ENABLE         0       // low active
#define CHIP_SEL_DISABLE        1

#endif	/* SPI_H */