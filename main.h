#ifndef _MAIN_H
#define	_MAIN_H

#define		CPU_FCY	40 				// for LCD elay referewnce
#ifndef Fcy
#define Fcy     CPU_FCY*1000000
#endif
#define Delay_200uS_Cnt  (Fcy * 0.02) / 1080

#define Setting_mode 1
#define Running_mode 0
#define OUTPUT 0

void delay_150ns(unsigned int i);
/* pwm */
int initPWM(void);
void initMP3911CLK(void);

#endif	/* _MAIN_H */