#include <xc.h>
#include <string.h>
#include <math.h>
#include "main.h"
#include "timer.h"
#include "spi.h"
#include "mcp3911.h"

unsigned char configReg[CONFIG_REG_QTY] = {0};
int MP3911ready2read = 0, MCP3911_BufCnt = 0;
long    MP3911_dataBuf_CH0[300] = {0},
        MP3911_dataBuf_CH1[300] = {0};
float rms[256] = {0},voltbuf[256] = {0};
unsigned char rmscnt = 0,voltcnt = 0;

ElectricalInfo MCP3911_CH0, MCP3911_CH1;

const char MCP3911_ConfigSetting[CONFIG_REG_QTY] = {
    0x00, //PHASE 
    0x00, //PHASE
    0xC1, //GAIN
    0x00, //status
    0xBE, //status
    0x27, //config PRE<15:14> OSR<13:11>
    0xC4, //config
    0x00, //offset0_0
    0x00, //offset0_1
    0x00, //offset0_2
    0x00, //gain0_1
    0x00, //gain0_2
    0x00, //gain0_3
    0xFE, //offset1_0
    0xF0, //offset1_1
    0x00, //offset1_2
    0x26, //gain1_1
    0xA0, //gain1_2
    0x00, //gain1_3
    0x42,
};

void MP3911_CheckStatus(void)
{
    int i = 0;
    unsigned char temp = 0;

    CS_MCP3911 = CHIP_SEL_ENABLE;
    delay_150ns(10);
    writeSPI2Byte(0x06 << 1 | 1);
    delay_150ns(10);
    temp = readSPI2Byte();
    while (i < CONFIG_REG_QTY) {
        temp = readSPI2Byte();
        configReg[i++] = temp;
        //delay_us(10);
    }
    delay_150ns(20);
    CS_MCP3911 = CHIP_SEL_DISABLE;
}

void initMCP3911(void)
{
    int i = 0;

    CS_MCP3911_STATUS = OUTPUT;
    CS_MCP3911 = CHIP_SEL_DISABLE;
    delay_150ns(10000); //Wait bus for becoming stable

    /* Step 1: Set both two ADC channels to be reset mode. It is recommended in manual 6.7.2 */
    CS_MCP3911 = CHIP_SEL_ENABLE;
    delay_150ns(10); //There must be more then 25ns between CS pull-low and SCK rising-edge
    writeSPI2Byte((ADDR_CONFIG + 1) << 1);
    delay_150ns(10);
    writeSPI2Byte(0xC0); //CONFIG<7:6>: RESET<1:0>=0b11(= Both CH0 and CH1 ADC are in reset mode)
    delay_150ns(20); //Wait for data transmition
    CS_MCP3911 = CHIP_SEL_DISABLE;

    delay_150ns(10000); //Wait for MCP3911 being stable

    CS_MCP3911 = CHIP_SEL_ENABLE;
    delay_150ns(10);
    writeSPI2Byte(ADDR_PHASE << 1); //address of PHASE       
    delay_150ns(10);
    for (i = 0; i < CONFIG_REG_QTY - 1; i++) { //There is -1 because I don't adjust MOD(0x06)
        writeSPI2Byte(MCP3911_ConfigSetting[i]);
    }
    delay_150ns(20);
    CS_MCP3911 = CHIP_SEL_DISABLE;

    delay_150ns(40000);

    CS_MCP3911 = CHIP_SEL_ENABLE;
    delay_150ns(10);
    writeSPI2Byte(0x0D << 1);
    writeSPI2Byte(0x02);
    delay_150ns(20);
    CS_MCP3911 = CHIP_SEL_DISABLE;
    delay_150ns(40000);

    MP3911_CheckStatus();
    delay_150ns(40000);

    TRISBbits.TRISB1 = 1;
    CNEN1bits.CN3IE = 1;
    IFS1bits.CNIF = 0;
    IEC1bits.CNIE = 1;

}

void MP3911_readVolt(void)
{
    int i = 0;
    char buf[12] = {0}, trash = 0;
    long temp = 0;

    CS_MCP3911 = CHIP_SEL_ENABLE;
    delay_150ns(10);
    writeSPI2Byte(0x00 << 1 | 1);
    delay_150ns(10);
    trash = readSPI2Byte();
    while (i < 6) {
        writeSPI2Byte(0x00);
        buf[i++] = readSPI2Byte();
    }
    delay_150ns(20);
    CS_MCP3911 = CHIP_SEL_DISABLE;

    temp |= (((long) buf[0] & 0xFF) << 16);
    temp |= (((long) buf[1] & 0xFF) << 8);
    temp |= (long) buf[2] & 0xFF;
    if (temp & 0x800000) {
        temp = 0xFFFFFF - temp + 1;
        temp = temp & 0xFFFFFF;
        temp = temp * (-1);
    }
    if (MCP3911_CH0.BufCnt >= 300) {
        MCP3911_CH0.BufCnt = 0;
    }
    
    MP3911_dataBuf_CH0[MCP3911_CH0.BufCnt] = temp;
    temp = 0;

    temp |= (((long) buf[3] & 0xFF) << 16);
    temp |= (((long) buf[4] & 0xFF) << 8);
    temp |= (long) buf[5] & 0xFF;
    if (temp & 0x800000) {
        temp = 0xFFFFFF - temp + 1;
        temp = temp & 0xFFFFFF;
        temp = temp * (-1);
    }
    MP3911_dataBuf_CH1[MCP3911_CH1.BufCnt] = temp;

    if (MCP3911_CH1.BufCnt >= 300) {
        MCP3911_CH1.BufCnt = 0;
    }
}

void MP3911_InfoCalc(void)
{
    static float temp0 = 0,temp1 = 0;
    float volt = 0;
    float amp = 0;

    volt = (float)MP3911_dataBuf_CH0[MCP3911_CH0.BufCnt]*300/6278258;
    amp = volt*0.31915;//3000/(4.7*2)
    amp = amp*amp;
    temp0 += amp;
    
    if(MP3911_dataBuf_CH0[MCP3911_CH0.BufCnt-1] > 0 && MP3911_dataBuf_CH0[MCP3911_CH0.BufCnt] <= 0){
        MCP3911_CH0.RecordFallingEdge[0] = MCP3911_CH0.RecordFallingEdge[1];
        MCP3911_CH0.RecordFallingEdge[1] = MCP3911_CH0.BufCnt;
        
        temp0 = temp0/(MCP3911_CH0.RecordFallingEdge[1] - MCP3911_CH0.RecordFallingEdge[0]);
        MCP3911_CH0.rms = sqrt(temp0);
        temp0 = 0;
    } 
    
    volt = (float)MP3911_dataBuf_CH1[MCP3911_CH1.BufCnt]*600/8180688;
    voltbuf[voltcnt++] = volt;
    amp = volt*0.15;
    amp = amp*amp;
    temp1 += amp;
    
    if(MP3911_dataBuf_CH1[MCP3911_CH1.BufCnt-1] > 0 && MP3911_dataBuf_CH1[MCP3911_CH1.BufCnt] <= 0){
        MCP3911_CH1.RecordFallingEdge[0] = MCP3911_CH1.RecordFallingEdge[1];
        MCP3911_CH1.RecordFallingEdge[1] = MCP3911_CH1.BufCnt;
        
        temp1 = temp1/(MCP3911_CH1.RecordFallingEdge[1] - MCP3911_CH1.RecordFallingEdge[0]);
        MCP3911_CH1.rms = sqrt(temp1);
        rms[rmscnt++] = MCP3911_CH1.rms;
        temp1 = 0;
    } 
}

void __attribute__((interrupt, auto_psv)) _CNInterrupt(void)
{
    IFS1bits.CNIF = 0; // Clear CN interrupt
    MP3911ready2read++;
}