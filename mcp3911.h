
#ifndef MCP3911_H
#define	MCP3911_H

#include <xc.h>

#define CS_MCP3911 LATBbits.LATB0
#define CS_MCP3911_STATUS TRISBbits.TRISB0

#define CONFIG_REG_QTY  21
#define ADDR_CHANNEL0    0x00
#define ADDR_CHANNEL1    0x03
#define ADDR_MOD         0x06
#define ADDR_PHASE       0x07
#define ADDR_GAIN        0x09
#define ADDR_STATUS      0x0A
#define ADDR_CONFIG      0x0C
#define ADDR_OFFCAL_CH0  0x0E
#define ADDR_GAINCAL_CH0 0x11
#define ADDR_OFFCAL_CH1  0x14
#define ADDR_GAINCAL_CH1 0x17
#define ADDR_VERCAL      0x1A

typedef struct{
    float period;
    float freq;
    float rms;
    int RecordFallingEdge[2];
    int BufCnt;
}ElectricalInfo;

void initMCP3911(void);
void MP3911_readVolt(void);
void MP3911_InfoCalc(void);

extern int MP3911ready2read,MCP3911_BufCnt;
extern long MP3911_dataBuf[300];
extern ElectricalInfo MCP3911_CH0,MCP3911_CH1;
#endif	/* MCP3911_H */

